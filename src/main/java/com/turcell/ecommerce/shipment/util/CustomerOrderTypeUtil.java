package com.turcell.ecommerce.shipment.util;

import com.turcell.ecommerce.shipment.model.dto.type.CustomerOrderType;
import com.turcell.ecommerce.shipment.model.entity.CustomerOrder;
import com.turcell.ecommerce.shipment.model.enums.CargoCompany;

import java.util.ArrayList;
import java.util.List;

public class CustomerOrderTypeUtil {

    /**
     *
     * @param customerOrderType
     * @return
     */
    public static CustomerOrder convertCustomerOrderTypeToCustomerOrder(CustomerOrderType customerOrderType){
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setCargoEventExplanation(customerOrderType.getCargoEventExplanation());
        customerOrder.setShipmentStatus(customerOrderType.getShipmentStatus());
        customerOrder.setCreateDate(customerOrderType.getCreateDate());
        customerOrder.setCargoCompany(customerOrderType.getCargoCompany());
        customerOrder.setCity(customerOrderType.getCity());
        customerOrder.setCustomerAddress(customerOrderType.getCustomerAddress());
        customerOrder.setCustomerName(customerOrderType.getCustomerName());
        customerOrder.setDeliveredDate(customerOrderType.getDeliveredDate());
        customerOrder.setOrderId(customerOrderType.getOrderId());
        customerOrder.setPhone(customerOrderType.getPhone());
        customerOrder.setTown(customerOrderType.getTown());
        customerOrder.setUpdateDate(customerOrderType.getUpdateDate());
        return customerOrder;
    }

    /**
     *
     * @param customerOrder
     * @return
     */
    public static CustomerOrderType convertCustomerOrderToCustomerOrderType(CustomerOrder customerOrder){
        CustomerOrderType customerOrderType = new CustomerOrderType();
        customerOrderType.setCargoEventExplanation(customerOrder.getCargoEventExplanation());
        customerOrderType.setShipmentStatus(customerOrder.getShipmentStatus());
        customerOrderType.setCreateDate(customerOrder.getCreateDate());
        customerOrderType.setCargoCompany(customerOrder.getCargoCompany());
        customerOrderType.setCargoCompanyName(CargoCompany.getWithCargoId(customerOrder.getCargoCompany()).getCargoName());
        customerOrderType.setCity(customerOrder.getCity());
        customerOrderType.setCustomerAddress(customerOrder.getCustomerAddress());
        customerOrderType.setCustomerName(customerOrder.getCustomerName());
        customerOrderType.setDeliveredDate(customerOrder.getDeliveredDate());
        customerOrderType.setOrderId(customerOrder.getOrderId());
        customerOrderType.setPhone(customerOrder.getPhone());
        customerOrderType.setTown(customerOrder.getTown());
        customerOrderType.setUpdateDate(customerOrder.getUpdateDate());
        return customerOrderType;
    }

    /**
     *
     * @param customerOrderList
     * @return
     */
    public static List<CustomerOrderType> convertCustomerOrderToCustomerOrderType(List<CustomerOrder> customerOrderList){
        List<CustomerOrderType> customerOrderTypeList = new ArrayList<>();
        if(customerOrderList != null) {
            for (CustomerOrder customerOrder : customerOrderList) {
                customerOrderTypeList.add(convertCustomerOrderToCustomerOrderType(customerOrder));
            }
        }
        return customerOrderTypeList;
    }
}
