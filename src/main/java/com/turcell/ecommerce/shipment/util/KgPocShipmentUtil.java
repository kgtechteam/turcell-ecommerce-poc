package com.turcell.ecommerce.shipment.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.turcell.ecommerce.shipment.model.entity.ServiceLog;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KgPocShipmentUtil {

    public static void convertJson(final ServiceLog serviceLog, final Object request, final Object response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        if (request != null) {
            try {
                serviceLog.setInParameters(mapper.writeValueAsString(request));
            } catch (Exception e) {
                log.error("setInParameters: " + e.getMessage(), e);
            }
        }
        if (response != null) {
            try {
                serviceLog.setOutParameters(mapper.writeValueAsString(response));
            } catch (Exception e) {
                log.error("setOutParameters: " + e.getMessage(), e);
            }
        }
    }

    public static String toJson(final Object request) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        if (request != null) {
            try {
                return mapper.writeValueAsString(request);
            } catch (Exception e) {
                log.error("setInParameters: " + e.getMessage(), e);
                return "ERROR";
            }
        }
        return null;
    }


}
