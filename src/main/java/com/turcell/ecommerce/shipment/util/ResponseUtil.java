package com.turcell.ecommerce.shipment.util;

import com.turcell.ecommerce.shipment.exception.ShipmentException;
import com.turcell.ecommerce.shipment.model.dto.base.BaseResponse;
import com.turcell.ecommerce.shipment.model.enums.ErrorCodeAndDesc;

public class ResponseUtil {

    public static void addResultIntoResponseObj(BaseResponse response, ErrorCodeAndDesc desc){
        response.setResponseCode(desc.getErrorCode());
        response.setResponseDesc(desc.getErrorDesc());
    }

    public static void addResultIntoResponseObj(BaseResponse response, ShipmentException e){
        response.setResponseCode(e.getErrorCode());
        response.setResponseDesc(e.getMessage());
    }
}
