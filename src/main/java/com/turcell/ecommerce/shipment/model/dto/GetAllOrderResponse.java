package com.turcell.ecommerce.shipment.model.dto;

import com.turcell.ecommerce.shipment.model.dto.base.BaseResponse;
import com.turcell.ecommerce.shipment.model.dto.type.CustomerOrderType;
import lombok.Data;

import java.util.List;

@Data
public class GetAllOrderResponse extends BaseResponse {

    private List<CustomerOrderType> customerOrderTypes;

}
