package com.turcell.ecommerce.shipment.model.enums;

public enum ErrorCodeAndDesc {

    SUCCESS(1000, "Success"),
    ORDER_IS_NULL(1001, "Order can not be null in request"),
    DUPLICATE_ORDER_ID(1005, "Duplicate order id"),
    GENERAL_ERROR(-1, "General Error");

    private int errorCode;
    private String errorDesc;

    ErrorCodeAndDesc(int errorCode, String errorDesc){
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }
}
