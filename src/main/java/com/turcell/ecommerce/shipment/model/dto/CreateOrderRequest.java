package com.turcell.ecommerce.shipment.model.dto;

import com.turcell.ecommerce.shipment.model.dto.base.BaseRequest;
import com.turcell.ecommerce.shipment.model.dto.type.CustomerOrderType;
import lombok.Data;

@Data
public class CreateOrderRequest implements BaseRequest {

    CustomerOrderType customerOrderType;

}
