package com.turcell.ecommerce.shipment.model.dto.type;

import com.turcell.ecommerce.shipment.model.enums.CargoCompany;
import lombok.Data;

import java.util.Date;

@Data
public class CustomerOrderType {

    private String orderId;

    private String customerName;

    private String customerAddress;

    private String city;

    private String town;

    private String phone;

    private int cargoCompany;

    private String cargoCompanyName;

    private int shipmentStatus;

    private Date createDate;

    private Date deliveredDate;

    private Date updateDate;

    private String cargoEventExplanation;

}
