package com.turcell.ecommerce.shipment.model.enums;

public enum ShipmentStatus {

    DELIVERED(1),
    WAITING(0),
    CANCELED(-1),
    NOT_SHIP(99);

    private int status;

    ShipmentStatus(int status){
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

}
