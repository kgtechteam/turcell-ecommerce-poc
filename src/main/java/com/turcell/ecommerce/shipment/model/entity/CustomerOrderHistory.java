package com.turcell.ecommerce.shipment.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class CustomerOrderHistory {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    private String orderId;

    private String customerName;

    private String customerAddress;

    private String city;

    private String town;

    private String phone;

    private int cargoCompany;

    private int shipmentStatus;

    private Date orderCreateDate;

    private Date createDate;

    private Date deliveredDate;

    private Date updateDate;

    private String cargoEventExplanation;

}
