package com.turcell.ecommerce.shipment.model.dto.base;

import lombok.Data;

@Data
public class BaseResponse {

    private int responseCode;
    private String responseDesc;

}
