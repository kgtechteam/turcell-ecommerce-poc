package com.turcell.ecommerce.shipment.model.enums;

public enum CargoCompany {

    YURT_ICI_CARGO(1, "Yurtici Kargo"),
    OTHER(-1, "Other");

    private int cargoId;
    private String cargoName;

    CargoCompany(int id, String cargoName){
        this.cargoId = id;
        this.cargoName = cargoName;
    }

    public static CargoCompany getWithCargoId(int cargoId) {
        for (CargoCompany item : CargoCompany.values()) {
            if (item.getCargoId() == cargoId) {
                return item;
            }
        }
        return CargoCompany.OTHER;
    }

    public int getCargoId() {
        return cargoId;
    }

    public void setCargoId(int cargoId) {
        this.cargoId = cargoId;
    }

    public String getCargoName() {
        return cargoName;
    }

    public void setCargoName(String cargoName) {
        this.cargoName = cargoName;
    }
}
