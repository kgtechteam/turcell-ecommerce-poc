package com.turcell.ecommerce.shipment.model.dto;

import com.turcell.ecommerce.shipment.model.dto.base.BaseResponse;
import com.turcell.ecommerce.shipment.model.dto.type.CustomerOrderType;
import lombok.Data;

@Data
public class GetOrderResponse extends BaseResponse {

    private CustomerOrderType customerOrderType;

}
