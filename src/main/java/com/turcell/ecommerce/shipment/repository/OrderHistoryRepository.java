package com.turcell.ecommerce.shipment.repository;

import com.turcell.ecommerce.shipment.model.entity.CustomerOrderHistory;
import org.springframework.data.repository.CrudRepository;

public interface OrderHistoryRepository extends CrudRepository<CustomerOrderHistory,Integer> {
}
