package com.turcell.ecommerce.shipment.repository;

import com.turcell.ecommerce.shipment.model.entity.ServiceLog;
import org.springframework.data.repository.CrudRepository;

public interface ServiceLogRepository extends CrudRepository<ServiceLog,Integer> {
}
