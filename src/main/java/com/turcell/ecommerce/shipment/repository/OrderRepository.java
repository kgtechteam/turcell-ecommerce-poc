package com.turcell.ecommerce.shipment.repository;

import com.turcell.ecommerce.shipment.model.entity.CustomerOrder;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<CustomerOrder,Integer> {

    List<CustomerOrder> findByShipmentStatus(int shipmentStatus);

    CustomerOrder findByOrderId(String orderId);

}
