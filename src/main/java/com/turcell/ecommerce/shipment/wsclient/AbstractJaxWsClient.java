package com.turcell.ecommerce.shipment.wsclient;

import java.util.Map;
import javax.xml.ws.BindingProvider;


public abstract class AbstractJaxWsClient<PORT> {


    public AbstractJaxWsClient() {
    }

    public abstract String getServiceEndPoint();

    public abstract int getConnectTimeout();

    public abstract int getReadTimeout();

    protected abstract PORT createServicePort() throws Exception;

    public PORT getServicePort() throws Exception {
        PORT servicePort = this.createServicePort();
        this.prepareForCall((BindingProvider)servicePort);
        return servicePort;
    }

    protected void prepareForCall(BindingProvider servicePort) {
        Map<String, Object> requestContext = servicePort.getRequestContext();
        requestContext.put("javax.xml.ws.service.endpoint.address", this.getServiceEndPoint());
        requestContext.put("com.sun.xml.ws.connect.timeout", this.getConnectTimeout());
        requestContext.put("com.sun.xml.ws.request.timeout", this.getReadTimeout());
    }
}
