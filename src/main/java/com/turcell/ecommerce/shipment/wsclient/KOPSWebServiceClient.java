package com.turcell.ecommerce.shipment.wsclient;

import com.google.gson.JsonObject;
import com.turcell.ecommerce.shipment.services.activemq.ActiveMqSender;
import com.turcell.ecommerce.shipment.model.entity.CustomerOrder;
import com.turcell.ecommerce.shipment.model.entity.ServiceLog;
import com.turcell.ecommerce.shipment.util.KgPocShipmentUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tr.com.yurticikargo.shippingorderdispatcherservices.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class KOPSWebServiceClient extends AbstractJaxWsClient<ShippingOrderDispatcherServices>{


    @Autowired
    ActiveMqSender activeMqSender;

    public static final String KOPS_WEB_SERVICE = "KOPSWebService";
    public static final String CREATE_SHIPMENT = "createShipment";

    @Value( "${shipping.order.dispatcher.service.url}" )
    private String URL;

    @Value( "${shipping.order.dispatcher.service.username}" )
    private String USER_NAME;

    @Value( "${shipping.order.dispatcher.service.password}" )
    private String PASSWORD;

    @Value( "${shipping.order.dispatcher.service.language}" )
    private String LANGUAGE;

    @Value( "${shipping.order.dispatcher.service.read.timeout}" )
    private int READ_TIMEOUT;

    @Value( "${shipping.order.dispatcher.service.connection.timeout}" )
    private int CONN_TIMEOUT;

    @Value( "${shipping.order.dispatcher.service.order.pref}" )
    private String ORDER_ID_PREF;

    @Override
    public String getServiceEndPoint() {
        return URL;
    }

    @Override
    public int getConnectTimeout() {
        return CONN_TIMEOUT;
    }

    @Override
    public int getReadTimeout() {
        return READ_TIMEOUT;
    }

    @Override
    protected ShippingOrderDispatcherServices createServicePort() throws Exception {
        final KOPSWebServices kopsWebServices = new KOPSWebServices();

        final ShippingOrderDispatcherServices servicePort = kopsWebServices.getShippingOrderDispatcherServicesPort();
        return servicePort;
    }

    /**
     *
     * Create shipment to cargo firm
     *
     * @param order
     * @return
     */
    public ShippingOrderResultVO createShipment(CustomerOrder order) {
        return createShipment(order.getOrderId(), order.getCustomerName(), order.getCustomerAddress(), order.getCity(), order.getTown(), order.getPhone());
    }

    /**
     *
     * @param orderId
     * @param customerName
     * @param customerAddress
     * @param city
     * @param town
     * @param phone
     * @return
     */
    private ShippingOrderResultVO createShipment(String orderId, String customerName, String customerAddress, String city, String town, String phone){
        ServiceLog logService = new ServiceLog(new Date(), KOPS_WEB_SERVICE, CREATE_SHIPMENT);
        List<ShippingOrderVO> shippingOrderVOList = generateShippingOrderObj(orderId, customerName, customerAddress, city, town, phone);
        ShippingOrderResultVO response = null;
        try {
            response = getServicePort().createShipment(USER_NAME, PASSWORD, LANGUAGE, shippingOrderVOList);
        } catch (Exception e) {
            log.error("ERROR Create Shipment method: ", e.getMessage(), e);
        } finally {
            String requestJson = generateCreateShipmentRequestString(shippingOrderVOList);
            logToDatabase(logService, requestJson, response);
        }
        return response;
    }

    /**
     *
     *
     * @param orderId
     * @param customerName
     * @param customerAddress
     * @param city
     * @param town
     * @param phone
     * @return
     */
    private List<ShippingOrderVO> generateShippingOrderObj(String orderId, String customerName, String customerAddress, String city, String town, String phone) {
        ShippingOrderVO shippingOrderVO = new ShippingOrderVO();
        String orderIdKey = ORDER_ID_PREF + orderId;
        shippingOrderVO.setCargoKey(orderIdKey);
        shippingOrderVO.setInvoiceKey(orderIdKey);
        shippingOrderVO.setReceiverAddress(customerAddress);
        shippingOrderVO.setReceiverCustName(customerName);
        shippingOrderVO.setCityName(city);
        shippingOrderVO.setTownName(town);
        shippingOrderVO.setReceiverPhone1(phone);
        List<ShippingOrderVO> shippingOrderVOList = new ArrayList<>();
        shippingOrderVOList.add(shippingOrderVO);
        return shippingOrderVOList;
    }

    /**
     *
     * @param log
     * @param request
     * @param response
     */
    private void logToDatabase(final ServiceLog log, final String request, final ExtendedBaseResultVO response) {
        log.setOutDate(new Date());
        log.setHeaderApplication("WS_CLIENT_KOPS_WEBSERVICE");
        if (response != null && response.getOutFlag() != null) {
            log.setResultCode(response.getOutFlag());
            log.setResultDesc(response.getOutResult());
        }
        KgPocShipmentUtil.convertJson(log, request, response);
        activeMqSender.sendLogService(log);
    }

    /**
     *
     * @param shippingOrderVOList
     * @return
     */
    private String generateCreateShipmentRequestString(List<ShippingOrderVO> shippingOrderVOList) {
        JsonObject formDetailsJson = new JsonObject();
        formDetailsJson.addProperty("userName", USER_NAME);
        formDetailsJson.addProperty("language", LANGUAGE);
        formDetailsJson.addProperty("shippingOrderVOList", KgPocShipmentUtil.toJson(shippingOrderVOList));
        return formDetailsJson.toString();
    }

}
