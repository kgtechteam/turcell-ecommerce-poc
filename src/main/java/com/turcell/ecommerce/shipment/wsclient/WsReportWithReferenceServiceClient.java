package com.turcell.ecommerce.shipment.wsclient;

import com.google.gson.JsonObject;
import com.turcell.ecommerce.shipment.services.activemq.ActiveMqSender;
import com.turcell.ecommerce.shipment.model.entity.ServiceLog;
import com.turcell.ecommerce.shipment.util.KgPocShipmentUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tr.com.yurticikargo.sswintegrationservices.*;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class WsReportWithReferenceServiceClient extends AbstractJaxWsClient<WsReportWithReferenceServices> {

    @Autowired
    ActiveMqSender activeMqSender;

    private static final String WS_REPORT_WITH_REFERENCE_SERVICE = "WsReportWithReferenceService";
    private static final String QUERY_SHIPMENT = "queryShipment";

    @Value( "${report.with.reference.service.url}" )
    private String URL;

    @Value( "${report.with.reference.service.username}" )
    private String USER_NAME;

    @Value( "${report.with.reference.service.password}" )
    private String PASSWORD;

    @Value( "${report.with.reference.service.language}" )
    private String LANGUAGE;

    @Value( "${report.with.reference.service.inv.cus.id}" )
    private String INV_CUS_ID;

    @Value( "${report.with.reference.service.field.name}" )
    private String FIELD_NAME;

    @Value( "${report.with.reference.service.cargo.lifecycle}" )
    private String CARGO_LIFECYCLE;

    @Value( "${report.with.reference.service.connection.timeout}" )
    private int CONN_TIMEOUT;

    @Value( "${report.with.reference.service.read.timeout}" )
    private int READ_TIMEOUT;

    @Override
    public String getServiceEndPoint() {
        return URL;
    }

    @Override
    public int getConnectTimeout() {
        return CONN_TIMEOUT;
    }

    @Override
    public int getReadTimeout() {
        return READ_TIMEOUT;
    }

    @Override
    protected WsReportWithReferenceServices createServicePort() throws Exception {
        final KOPSWebServices kopsWebServices = new KOPSWebServices();
        final WsReportWithReferenceServices servicePort = kopsWebServices.getWsReportWithReferenceServicesPort();
        return servicePort;
    }

    /**
     *
     * @param fieldValueArray
     * @return
     */
    public ShippingDataResponseVO queryShipping(List<String> fieldValueArray){
        return queryShipping(fieldValueArray, null, null, null, null);
    }

    /**
     *
     * @param fieldValueArray
     * @param docIdArray
     * @param startDate
     * @param endDate
     * @param dateParamType
     * @return
     */
    private ShippingDataResponseVO queryShipping(List<String> fieldValueArray, List<String> docIdArray, String startDate, String endDate, String dateParamType){

        ServiceLog logService = new ServiceLog(new Date(), WS_REPORT_WITH_REFERENCE_SERVICE, QUERY_SHIPMENT);
        CustParamsVO custParam = new CustParamsVO();
        custParam.getInvCustIdArray().add(INV_CUS_ID);
        ShippingDataResponseVO response = null;
        try {
            response = getServicePort().listInvDocumentInterfaceByReference(USER_NAME, PASSWORD, LANGUAGE, custParam, FIELD_NAME, fieldValueArray, docIdArray, startDate, endDate, dateParamType, CARGO_LIFECYCLE);
        } catch (Exception e) {
            log.error("ERROR Query Shipping method: ", e.getMessage(), e);
        } finally {
            String requestJson = generateQueryShippingJsonString(custParam, fieldValueArray, docIdArray, startDate, endDate, dateParamType);
            logToDatabase(logService, requestJson, response);
        }
        return response;
    }

    /**
     *
     * @param log
     * @param request
     * @param response
     */
    private void logToDatabase(final ServiceLog log, final String request, final BaseResultVO response) {
        log.setOutDate(new Date());
        log.setHeaderApplication("WS_CLIENT_WS_REPORT_WITH_REFERENCE_SERVICE");
        if (response != null && response.getOutFlag() != null) {
            log.setResultCode(response.getOutFlag());
            log.setResultDesc(response.getOutResult());
        }

        KgPocShipmentUtil.convertJson(log, request, response);
        activeMqSender.sendLogService(log);
    }

    /**
     *
     * @param custParam
     * @param fieldValueArray
     * @param docIdArray
     * @param startDate
     * @param endDate
     * @param dateParamType
     * @return
     */
    private String generateQueryShippingJsonString(CustParamsVO custParam, List<String> fieldValueArray, List<String> docIdArray, String startDate, String endDate, String dateParamType) {
        JsonObject formDetailsJson = new JsonObject();
        formDetailsJson.addProperty("userName", USER_NAME);
        formDetailsJson.addProperty("language", LANGUAGE);
        formDetailsJson.addProperty("fieldValueArray", KgPocShipmentUtil.toJson(fieldValueArray));
        formDetailsJson.addProperty("custParam", KgPocShipmentUtil.toJson(custParam));
        formDetailsJson.addProperty("fieldName", FIELD_NAME);
        formDetailsJson.addProperty("docIdArray", KgPocShipmentUtil.toJson(docIdArray));
        formDetailsJson.addProperty("startDate", startDate);
        formDetailsJson.addProperty("endDate", endDate);
        formDetailsJson.addProperty("dateParamType", dateParamType);
        formDetailsJson.addProperty("cargoLifecycle", CARGO_LIFECYCLE);
        return formDetailsJson.toString();
    }
}
