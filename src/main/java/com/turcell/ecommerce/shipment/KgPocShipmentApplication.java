package com.turcell.ecommerce.shipment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KgPocShipmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(KgPocShipmentApplication.class, args);
	}

}
