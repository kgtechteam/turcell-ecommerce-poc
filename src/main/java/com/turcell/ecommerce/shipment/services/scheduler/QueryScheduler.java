package com.turcell.ecommerce.shipment.services.scheduler;

import com.turcell.ecommerce.shipment.services.operation.QueryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class QueryScheduler {

    @Autowired
    QueryService queryService;

    /**
     * This job checks all shipments with Waiting statuses,
     *     then makes a service call to cargo firm
     *     updates the shipment status if needed
     */
    @Scheduled(cron="${cron.expression.query.order}", zone="Europe/Istanbul")
    public void checkShipmentStatus() {
        log.info("Schedule working");
        queryService.execute();
    }
}
