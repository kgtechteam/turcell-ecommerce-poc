package com.turcell.ecommerce.shipment.services;

import com.turcell.ecommerce.shipment.model.dto.base.BaseRequest;
import com.turcell.ecommerce.shipment.model.dto.base.BaseResponse;
import com.turcell.ecommerce.shipment.model.entity.ServiceLog;
import com.turcell.ecommerce.shipment.services.activemq.ActiveMqSender;
import com.turcell.ecommerce.shipment.util.KgPocShipmentUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class WsLogService {

    @Autowired
    ActiveMqSender activeMqSender;

    /**
     *
     * This method is used for post services
     *
     * @param request baseRequest
     * @param response baseResponse
     */
    public void createServiceLog(BaseRequest request, BaseResponse response, String serviceName, String serviceOperation){
        ServiceLog log = new ServiceLog();
        log.setOutDate(new Date());
        log.setServiceName(serviceName);
        log.setServiceOperation(serviceOperation);
        if (response != null && response.getResponseDesc() != null) {
            log.setResultCode(String.valueOf(response.getResponseCode()));
            log.setResultDesc(response.getResponseDesc());
            log.setHeaderApplication("REST_WEB_SERVICE_POST");
        }
        KgPocShipmentUtil.convertJson(log, request, response);
        activeMqSender.sendLogService(log);
    }

    /**
     *
     * This method is used for get services
     *
     * @param response baseResponse
     */
    public void createServiceLog(BaseResponse response, String serviceName, String serviceOperation){
        ServiceLog log = new ServiceLog();
        log.setOutDate(new Date());
        log.setServiceName(serviceName);
        log.setServiceOperation(serviceOperation);
        if (response != null && response.getResponseDesc() != null) {
            log.setResultCode(String.valueOf(response.getResponseCode()));
            log.setResultDesc(response.getResponseDesc());
            log.setHeaderApplication("REST_WEB_SERVICE_GET");
        }
        KgPocShipmentUtil.convertJson(log, null, response);
        activeMqSender.sendLogService(log);
    }
}
