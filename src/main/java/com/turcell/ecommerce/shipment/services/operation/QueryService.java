package com.turcell.ecommerce.shipment.services.operation;

import com.turcell.ecommerce.shipment.dao.OrderDAO;
import com.turcell.ecommerce.shipment.model.entity.CustomerOrder;
import com.turcell.ecommerce.shipment.model.enums.ShipmentStatus;
import com.turcell.ecommerce.shipment.repository.OrderRepository;
import com.turcell.ecommerce.shipment.wsclient.WsReportWithReferenceServiceClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.yurticikargo.sswintegrationservices.ShippingDataDetailVO;
import tr.com.yurticikargo.sswintegrationservices.ShippingDataResponseVO;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class QueryService {

    public static final String CARGO_EVENT_OK = "OK";
    public static final String REJECT_FLAG_NOT_REJECTED = "0";
    public static final String REJECT_FLAG_REJECTED = "1";

    @Autowired
    OrderDAO orderDAO;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    WsReportWithReferenceServiceClient reportWithReferenceService;

    /**
     *
     *This method finds all waiting status customer orders, than checks shipment status from cargo firm
     *
     */
    public void execute() {
        try {
            List<CustomerOrder> customerOrders = orderRepository.findByShipmentStatus(ShipmentStatus.WAITING.getStatus());
            for (CustomerOrder order : customerOrders) {
                checkShipmentStatus(order);
            }
        } catch (Exception e){
            log.error("Query shipment has error: ", e.getMessage(), e);
        }
    }

    /**
     *
     * Check and update shipment status
     *
     * @param order
     * @return
     */
    private void checkShipmentStatus(CustomerOrder order) {
        try {
            boolean anyThingChange = false;
            ShippingDataResponseVO response = reportWithReferenceService.queryShipping(Arrays.asList(order.getOrderId()));
            if (response != null) {
                List<ShippingDataDetailVO> shippingDataDetailVOList = response.getShippingDataDetailVOArray();
                if (shippingDataDetailVOList != null) {
                    ShippingDataDetailVO shippingDataDetailVO = shippingDataDetailVOList.get(0);
                    if (CARGO_EVENT_OK.equals(shippingDataDetailVO.getCargoEventId()) && REJECT_FLAG_NOT_REJECTED.equals(shippingDataDetailVO.getRejectFlag())) {
                        order.setShipmentStatus(ShipmentStatus.DELIVERED.getStatus());
                        anyThingChange = true;
                    } else if (CARGO_EVENT_OK.equals(shippingDataDetailVO.getCargoEventId()) && REJECT_FLAG_REJECTED.equals(shippingDataDetailVO.getRejectFlag())) {
                        order.setShipmentStatus(ShipmentStatus.CANCELED.getStatus());
                        anyThingChange = true;
                    }
                    if (shippingDataDetailVO.getCargoEventExplanation() != null) {
                        order.setCargoEventExplanation(shippingDataDetailVO.getCargoEventExplanation());
                        anyThingChange = true;
                    }
                }
            }
            if (anyThingChange) {
                orderDAO.saveOrUpdate(order);
            }
        } catch (Exception e){
            log.error("Query shipment has error Order Id {}: ", order.getOrderId(), e.getMessage(), e);
        }
    }

}
