package com.turcell.ecommerce.shipment.services.operation;

import com.turcell.ecommerce.shipment.dao.OrderDAO;
import com.turcell.ecommerce.shipment.exception.ShipmentException;
import com.turcell.ecommerce.shipment.model.dto.CreateOrderRequest;
import com.turcell.ecommerce.shipment.model.dto.CreateOrderResponse;
import com.turcell.ecommerce.shipment.model.dto.GetAllOrderResponse;
import com.turcell.ecommerce.shipment.model.dto.GetOrderResponse;
import com.turcell.ecommerce.shipment.model.dto.type.CustomerOrderType;
import com.turcell.ecommerce.shipment.model.entity.CustomerOrder;
import com.turcell.ecommerce.shipment.model.enums.ErrorCodeAndDesc;
import com.turcell.ecommerce.shipment.model.enums.ShipmentStatus;

import com.turcell.ecommerce.shipment.repository.OrderHistoryRepository;
import com.turcell.ecommerce.shipment.repository.OrderRepository;
import com.turcell.ecommerce.shipment.services.WsLogService;
import com.turcell.ecommerce.shipment.util.CustomerOrderTypeUtil;
import com.turcell.ecommerce.shipment.util.ResponseUtil;
import com.turcell.ecommerce.shipment.wsclient.KOPSWebServiceClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.yurticikargo.shippingorderdispatcherservices.ShippingOrderResultVO;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class OrderService {

    private static final String SUCCESS = "0";
    private static final String ORDER_SERVICE = "ORDER_SERVICE";

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    OrderHistoryRepository orderHistoryRepository;

    @Autowired
    KOPSWebServiceClient kopsWebService;

    @Autowired
    WsLogService logService;

    @Autowired
    OrderDAO orderDAO;

    /**
     *
     * This method creates new order and send order to cargo firm
     *
     * @param request createOrderRequest
     * @return createOrderResponse
     */
    public CreateOrderResponse createAndShipOrder(CreateOrderRequest request) {
        CreateOrderResponse response = new CreateOrderResponse();
        try {
            validateRequest(request);
            CustomerOrder order = CustomerOrderTypeUtil.convertCustomerOrderTypeToCustomerOrder(request.getCustomerOrderType());
            order.setShipmentStatus(ShipmentStatus.NOT_SHIP.getStatus());
            orderDAO.saveOrder(order);
            //Cargo firms webservice call
            ShippingOrderResultVO orderResultVO = kopsWebService.createShipment(order);
            //Cargo is sent
            if(orderResultVO != null && SUCCESS.equals(orderResultVO.getOutFlag())){
                log.info("Order successfully sent");
                order.setShipmentStatus(ShipmentStatus.WAITING.getStatus());
                orderDAO.saveOrUpdate(order);
                log.info("Order successfully saved");
            }
            response.setCustomerOrderType(CustomerOrderTypeUtil.convertCustomerOrderToCustomerOrderType(order));
            ResponseUtil.addResultIntoResponseObj(response, ErrorCodeAndDesc.SUCCESS);
        }catch (ShipmentException e){
            ResponseUtil.addResultIntoResponseObj(response, e);
        }catch (Exception e){
            ResponseUtil.addResultIntoResponseObj(response, ErrorCodeAndDesc.GENERAL_ERROR);
        } finally {
            logService.createServiceLog(request,response,ORDER_SERVICE, "CREATE_AND_SHIP_ORDER");
        }

        return response;
    }

    private void validateRequest(CreateOrderRequest request) throws ShipmentException {
        if(request == null || request.getCustomerOrderType() == null){
            throw new ShipmentException(ErrorCodeAndDesc.ORDER_IS_NULL.getErrorCode(), ErrorCodeAndDesc.ORDER_IS_NULL.getErrorDesc());
        }
        CustomerOrder customerOrder = orderRepository.findByOrderId(request.getCustomerOrderType().getOrderId());
        if(customerOrder != null){
            throw new ShipmentException(ErrorCodeAndDesc.DUPLICATE_ORDER_ID.getErrorCode(), ErrorCodeAndDesc.DUPLICATE_ORDER_ID.getErrorDesc());
        }
    }

    /**
     *
     * This method gets all existing orders
     *
     * @return getAllOrderResponse
     */
    public GetAllOrderResponse getAllOrders() {
        GetAllOrderResponse response = new GetAllOrderResponse();
        try {
            List<CustomerOrder> orders = new ArrayList<>();
            orderRepository.findAll().forEach(orders::add);
            List<CustomerOrderType> customerOrderTypeList = CustomerOrderTypeUtil.convertCustomerOrderToCustomerOrderType(orders);
            response.setCustomerOrderTypes(customerOrderTypeList);
            ResponseUtil.addResultIntoResponseObj(response, ErrorCodeAndDesc.SUCCESS);
        } catch (Exception e){
            ResponseUtil.addResultIntoResponseObj(response, ErrorCodeAndDesc.GENERAL_ERROR);
        } finally {
            logService.createServiceLog(response, ORDER_SERVICE, "GET_ALL_ORDERS");
        }
        return response;
    }

    /**
     *
     * This method gets order with given order id
     *
     * @param orderId
     * @return getOrderResponse
     */
    public GetOrderResponse getOrderByOrderId(String orderId) {
        GetOrderResponse response = new GetOrderResponse();
        try {
            CustomerOrder order = orderRepository.findByOrderId(orderId);
            response.setCustomerOrderType(CustomerOrderTypeUtil.convertCustomerOrderToCustomerOrderType(order));
            ResponseUtil.addResultIntoResponseObj(response, ErrorCodeAndDesc.SUCCESS);
        } catch (Exception e){
            ResponseUtil.addResultIntoResponseObj(response, ErrorCodeAndDesc.GENERAL_ERROR);
        } finally {
            logService.createServiceLog(response, ORDER_SERVICE, "GET_ORDER_BY_ORDER_ID");
        }
        return response;
    }
}
