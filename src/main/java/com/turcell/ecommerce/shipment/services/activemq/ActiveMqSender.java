package com.turcell.ecommerce.shipment.services.activemq;

import com.turcell.ecommerce.shipment.model.entity.ServiceLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import static com.turcell.ecommerce.shipment.config.ActiveMQConfig.LOG_SERVICE_QUEUE;

@Slf4j
@Service
public class ActiveMqSender {

    @Autowired
    private JmsTemplate jmsTemplate;

    /**
     *
     * @param serviceLog
     */
    public void sendLogService(ServiceLog serviceLog) {
        log.info("sending with convertAndSend() to queue <" + serviceLog + ">");
        jmsTemplate.convertAndSend(LOG_SERVICE_QUEUE, serviceLog);
    }
}