package com.turcell.ecommerce.shipment.controller;

import com.turcell.ecommerce.shipment.model.dto.CreateOrderRequest;
import com.turcell.ecommerce.shipment.model.dto.CreateOrderResponse;
import com.turcell.ecommerce.shipment.model.dto.GetAllOrderResponse;
import com.turcell.ecommerce.shipment.model.dto.GetOrderResponse;
import com.turcell.ecommerce.shipment.services.operation.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    /**
     *
     * This method gets all existing orders
     *
     * @return
     */
    @GetMapping("/orders")
    private GetAllOrderResponse getAllOrders() {
        return orderService.getAllOrders();
    }

    /**
     *
     * This method gets order with given order id
     *
     * @param orderId
     * @return
     */
    @GetMapping("/orders/{orderId}")
    private GetOrderResponse getOrderByOrderId(@PathVariable("orderId") String  orderId) {
        return orderService.getOrderByOrderId(orderId);
    }

    /**
     *
     * This method creates new order and send order to cargo firm
     *
     * @param request
     * @return
     */
    @PostMapping("/orders")
    private CreateOrderResponse createOrder(@RequestBody CreateOrderRequest request) {
        return orderService.createAndShipOrder(request);
    }
}
