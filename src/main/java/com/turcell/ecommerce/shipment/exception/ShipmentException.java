package com.turcell.ecommerce.shipment.exception;

public class ShipmentException extends Exception {

    private static final long serialVersionUID = 1L;

    private int errorCode;

    public ShipmentException(String errorMessage) {
        super(errorMessage);
    }

    public ShipmentException(int errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
