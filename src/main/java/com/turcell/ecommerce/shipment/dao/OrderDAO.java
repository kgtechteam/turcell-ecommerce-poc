package com.turcell.ecommerce.shipment.dao;

import com.turcell.ecommerce.shipment.model.entity.CustomerOrder;
import com.turcell.ecommerce.shipment.model.entity.CustomerOrderHistory;
import com.turcell.ecommerce.shipment.repository.OrderHistoryRepository;
import com.turcell.ecommerce.shipment.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OrderDAO {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    OrderHistoryRepository orderHistoryRepository;

    public void  saveOrder(CustomerOrder order) {
        order.setCreateDate(new Date());
        saveOrUpdate(order);
    }

    public void saveOrUpdate(CustomerOrder order) {
        orderRepository.save(order);
        saveOrderHistory(order);
    }

    public void saveOrderHistory(CustomerOrder order) {
        CustomerOrderHistory customerOrderHistory = generateOrderHistory(order);
        saveOrUpdate(customerOrderHistory);
    }

    public void saveOrUpdate(CustomerOrderHistory customerOrderHistory) {
        orderHistoryRepository.save(customerOrderHistory);
    }

    private CustomerOrderHistory generateOrderHistory(CustomerOrder order) {

        CustomerOrderHistory history = new CustomerOrderHistory();
        history.setCargoCompany(order.getCargoCompany());
        history.setCity(order.getCity());
        history.setCreateDate(new Date());
        history.setOrderCreateDate(order.getCreateDate());
        history.setCustomerAddress(order.getCustomerAddress());
        history.setCustomerName(order.getCustomerName());
        history.setDeliveredDate(order.getDeliveredDate());
        history.setOrderId(order.getOrderId());
        history.setPhone(order.getPhone());
        history.setShipmentStatus(order.getShipmentStatus());
        history.setTown(order.getTown());
        history.setUpdateDate(order.getUpdateDate());
        history.setCargoEventExplanation(order.getCargoEventExplanation());
        return history;
    }
}
