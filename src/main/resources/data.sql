
INSERT INTO Customer_Order (id, order_id, customer_name, customer_address, city, town, phone, cargo_company, shipment_status, create_date, delivered_date, update_date) VALUES
  (-1, '00001', 'John Doe', 'Kg Technology Istanbul', 'Istanbul', 'Üsküdar', '5541234567', 1, 0, SYSDATE, NULL, NULL),
  (-2, '00002', 'Alice Brown', 'Kg Technology Istanbul', 'Istanbul', 'Üsküdar', '5321234567', 1, 0, SYSDATE, NULL, NULL),
  (-3, '00003', 'Bob Smith', 'Kg Technology Istanbul', 'Istanbul', 'Üsküdar', '5301234567', -1, 0, SYSDATE, NULL, NULL);


  INSERT INTO Customer_Order_History (id, order_id, customer_name, customer_address, city, town, phone, cargo_company, shipment_status, create_date, delivered_date, update_date) VALUES
    (-1, '00001', 'John Doe', 'Kg Technology Istanbul', 'Istanbul', 'Üsküdar', '5541234567', 1, 0, SYSDATE, NULL, NULL),
    (-2, '00002', 'Alice Brown', 'Kg Technology Istanbul', 'Istanbul', 'Üsküdar', '5321234567', 1, 0, SYSDATE, NULL, NULL),
    (-3, '00003', 'Bob Smith', 'Kg Technology Istanbul', 'Istanbul', 'Üsküdar', '5301234567', -1, 0, SYSDATE, NULL, NULL);


   COMMIT;