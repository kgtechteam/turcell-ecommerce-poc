package com.turcell.ecommerce.shipment.shipmenttest;

import com.turcell.ecommerce.shipment.model.dto.CreateOrderRequest;
import com.turcell.ecommerce.shipment.model.dto.type.CustomerOrderType;
import com.turcell.ecommerce.shipment.model.entity.CustomerOrder;
import com.turcell.ecommerce.shipment.model.enums.CargoCompany;
import com.turcell.ecommerce.shipment.model.enums.ShipmentStatus;
import com.turcell.ecommerce.shipment.repository.OrderRepository;
import com.turcell.ecommerce.shipment.services.operation.OrderService;
import com.turcell.ecommerce.shipment.services.operation.QueryService;
import com.turcell.ecommerce.shipment.util.ShipmentTestUtil;
import com.turcell.ecommerce.shipment.wsclient.KOPSWebServiceClient;
import com.turcell.ecommerce.shipment.wsclient.WsReportWithReferenceServiceClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tr.com.yurticikargo.shippingorderdispatcherservices.ShippingOrderResultVO;
import tr.com.yurticikargo.sswintegrationservices.ShippingDataDetailVO;
import tr.com.yurticikargo.sswintegrationservices.ShippingDataResponseVO;

import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class ShipmentSuccessTest {

    @Mock
    WsReportWithReferenceServiceClient queryShipmentMock;

    @Mock
    KOPSWebServiceClient createOrderClientMock;

    @Autowired
    @InjectMocks
    OrderService orderService;

    @Autowired
    @InjectMocks
    QueryService queryService;

    @Autowired
    OrderRepository orderRepository;


    /**
     *  Mock createShipment service to return success result
     *  Mock queryShipment service to return  delivered status
     */
    @BeforeEach
    void setMockOutput() {
        MockitoAnnotations.initMocks(this);

        ShippingDataResponseVO successQueryResult = new ShippingDataResponseVO();
        ShippingDataDetailVO successQueryDetail = new ShippingDataDetailVO();
        successQueryDetail.setCargoEventId("OK");
        successQueryDetail.setRejectFlag("0");
        successQueryResult.getShippingDataDetailVOArray().add(successQueryDetail);

        when(queryShipmentMock.queryShipping(any(List.class)))
                .thenReturn(successQueryResult);


        ShippingOrderResultVO successCreateShipmentResult = new ShippingOrderResultVO();
        successCreateShipmentResult.setOutFlag("0");
        successCreateShipmentResult.setOutResult("Başarılı.");

        when(createOrderClientMock.createShipment(any(CustomerOrder.class)))
                .thenReturn(successCreateShipmentResult);
    }

    @Test
    void shipmentDeliveredTest(){
        String orderId = "TestSuccessOrder";
        //create and ship order
        CreateOrderRequest order = ShipmentTestUtil.createOrderRequest(orderId);
        orderService.createAndShipOrder(order);

        //check if order created
        CustomerOrder savedOrder = orderRepository.findByOrderId(orderId);
        Assertions.assertEquals(savedOrder.getShipmentStatus(), ShipmentStatus.WAITING.getStatus(), "Cretea shipment test failed");

        //check if order shipment status is delivered
        queryService.execute();
        CustomerOrder deliveredOrder = orderRepository.findByOrderId(orderId);
        Assertions.assertEquals(deliveredOrder.getShipmentStatus(), ShipmentStatus.DELIVERED.getStatus(), "Cretea shipment test failed");

    }

}
