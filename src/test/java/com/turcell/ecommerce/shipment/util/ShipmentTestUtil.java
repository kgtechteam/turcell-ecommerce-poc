package com.turcell.ecommerce.shipment.util;

import com.turcell.ecommerce.shipment.model.dto.CreateOrderRequest;
import com.turcell.ecommerce.shipment.model.dto.type.CustomerOrderType;
import com.turcell.ecommerce.shipment.model.enums.CargoCompany;

import java.util.Date;

public class ShipmentTestUtil {

    public  static CreateOrderRequest createOrderRequest(String orderId){
        CreateOrderRequest request = new CreateOrderRequest();

        CustomerOrderType order = new CustomerOrderType();
        order.setShipmentStatus(0);
        order.setCreateDate(new Date());
        order.setCargoCompany(CargoCompany.YURT_ICI_CARGO.getCargoId());
        order.setCity("Test City");
        order.setCustomerAddress("Test Address");
        order.setCustomerName("Test Name");
        order.setOrderId(orderId);
        order.setPhone("Test Phone");
        order.setTown("Test Town");

        request.setCustomerOrderType(order);

        return request;
    }
}
